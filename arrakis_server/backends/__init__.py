# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

from enum import IntEnum, auto


class BackendType(IntEnum):
    NONE = auto()
    MOCK = auto()
    KAFKA = auto()
    NDS2 = auto()
