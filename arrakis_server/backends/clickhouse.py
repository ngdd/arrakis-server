# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

import time
from collections.abc import Iterable, Iterator

import gpstime
import numpy
import pyarrow
from arrakis import SeriesBlock, Time
from daqd_clickhouse import sql
from daqd_clickhouse.grpc.client import DEFAULT_PORT, ClickHouseGRPCClient

from ..channel import Channel
from ..traits import ServerBackend


class ClickHouseBackend(ServerBackend):
    """Server backend serving timeseries data from ClickHouse."""

    def __init__(self, server_url: str):
        self._server_url = server_url
        if ":" in server_url:
            self._host, self._port = server_url.split(":", 1)
        else:
            self._host = server_url
            self._port = DEFAULT_PORT

    def find(
        self,
        *,
        pattern: str,
        data_type: list[str],
        min_rate: int,
        max_rate: int,
        publisher: list[str],
    ) -> Iterable[Channel]:
        """Retrieve metadata for the 'find' route."""
        with ClickHouseGRPCClient(
            host=self._host, port=self._port, database="timeseries"
        ) as conn:
            query = sql.select_metadata_by_pattern(
                pattern, data_type, min_rate, max_rate
            )
            response = conn.run_query(query)
            batches = [*response_to_record_batches(response)]
            return self._extract_channel_metadata(batches)

    def count(
        self,
        *,
        pattern: str,
        data_type: list[str],
        min_rate: int,
        max_rate: int,
        publisher: list[str],
    ) -> int:
        """Retrieve metadata for the 'count' route."""
        with ClickHouseGRPCClient(
            host=self._host, port=self._port, database="timeseries"
        ) as conn:
            query = sql.count_metadata_by_pattern(
                pattern, data_type, min_rate, max_rate
            )
            response = conn.run_query(query)
            count = 0
            for batch in response_to_record_batches(response):
                for _, entry in batch.to_pandas().iterrows():
                    count += entry["count"]
            return count

    def describe(self, *, channels: Iterable[str]) -> Iterable[Channel]:
        """Retrieve metadata for the 'describe' route."""
        batches = self._describe_channels(channels)
        return self._extract_channel_metadata(batches)

    def stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> Iterator[SeriesBlock]:
        """Retrieve timeseries data for the 'fetch' route."""
        # Query for channel metadata
        metadata = self._get_channel_metadata(channels)
        is_live = not start and not end
        if is_live:
            return self._generate_live_series(metadata)
        else:
            return self._generate_series(metadata, start, end)

    def _describe_channels(
        self, channels: Iterable[str]
    ) -> Iterator[pyarrow.RecordBatch]:
        with ClickHouseGRPCClient(
            host=self._host, port=self._port, database="timeseries"
        ) as conn:
            query = sql.select_metadata_by_channels(channels)
            response = conn.run_query(query)
            yield from response_to_record_batches(response)

    def _get_channel_metadata(self, channels: Iterable[str]) -> list[Channel]:
        batches = self._describe_channels(channels)
        return self._extract_channel_metadata(batches)

    @staticmethod
    def _extract_channel_metadata(
        batches: Iterable[pyarrow.RecordBatch],
    ) -> list[Channel]:
        channels = []
        for batch in batches:
            for _, entry in batch.to_pandas().iterrows():
                channel = Channel(
                    entry["channel"].decode("utf-8"),
                    data_type=numpy.dtype(entry["data_type"].decode("utf-8")),
                    sample_rate=entry["sample_rate"],
                )
                channels.append(channel)
        return channels

    def _generate_series(
        self, channels: Iterable[Channel], start: int, end: int
    ) -> Iterator[SeriesBlock]:
        channel_map = {channel.name: channel for channel in channels}
        with ClickHouseGRPCClient(
            host=self._host, port=self._port, database="timeseries"
        ) as conn:
            query = sql.select_data_by_channels(channels, start, end)
            response = conn.run_query(query)
            for batch in response_to_record_batches(response):
                yield SeriesBlock.from_row_batch(batch, channel_map)

    def _generate_live_series(
        self, channels: Iterable[Channel]
    ) -> Iterator[SeriesBlock]:
        dt = int((1 / 16) * Time.SECONDS)
        current = (int(gpstime.gpsnow() * Time.SECONDS) // dt) * dt
        channel_map = {channel.name: channel for channel in channels}

        # generate live data continuously
        while True:
            with ClickHouseGRPCClient(
                host=self._host, port=self._port, database="timeseries"
            ) as conn:
                query = sql.select_data_by_channels(channels, start=current)
                response = conn.run_query(query)
                for batch in response_to_record_batches(response):
                    block = SeriesBlock.from_row_batch(batch, channel_map)
                    current = block.time_ns
                    yield block

            # sleep for up to dt before querying for more data
            time.sleep(
                max((current - int(gpstime.gpsnow() * Time.SECONDS)) / Time.SECONDS, 0)
            )


def response_to_record_batches(
    response: Iterable[bytes],
) -> Iterator[pyarrow.RecordBatch]:
    schema = None
    for row in response:
        if not row:
            continue
        try:
            if schema:
                yield pyarrow.ipc.read_record_batch(row, schema)
            else:
                with pyarrow.ipc.open_stream(row) as reader:
                    schema = reader.schema
                    yield from read_all_batches(reader)
        except pyarrow.lib.ArrowInvalid:
            pass
        except EOFError:
            pass


def read_all_batches(
    reader: pyarrow.ipc.RecordBatchStreamReader,
) -> Iterator[pyarrow.RecordBatch]:
    while True:
        try:
            yield reader.read_next_batch()
        except StopIteration:
            return
