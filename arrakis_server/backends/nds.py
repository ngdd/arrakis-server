# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

from collections import defaultdict
from collections.abc import Iterable, Iterator
from typing import Any

import nds2
import numpy
from arrakis import SeriesBlock, Time

from ..channel import Channel, extract_channel_scope
from ..scope import Retention, ScopeInfo
from ..traits import ServerBackend

_NDS2_DATA_TYPE: dict[int, numpy.dtype] = {
    1: numpy.dtype(numpy.int16),
    2: numpy.dtype(numpy.int32),
    4: numpy.dtype(numpy.int64),
    8: numpy.dtype(numpy.float32),
    16: numpy.dtype(numpy.float64),
    64: numpy.dtype(numpy.uint32),
}


class NDS2Backend(ServerBackend):
    """Server backend serving timeseries data from NDS2."""

    def __init__(self, server_url: str):
        self._server_url = server_url
        if ":" in server_url:
            host, port = server_url.split(":", 1)
            self._params = nds2.parameters(host, int(port))
        else:
            self._params = nds2.parameters(server_url)

        # extract channel information from all channels
        nds_channels = nds2.find_channels(
            "*",
            nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_ONLINE,
            params=self._params,
        )

        # derive scopes from channel info
        scopes: dict[str, list[dict[str, Any]]] = defaultdict(list)
        for channel in nds_channels:
            domain, subsystem = extract_channel_scope(channel.name)
            scopes[domain].append({"subsystem": subsystem})

        self.scope_info = ScopeInfo(scopes, Retention())

    def find(
        self,
        *,
        pattern: str,
        data_type: list[str],
        min_rate: int,
        max_rate: int,
        publisher: list[str],
    ) -> Iterable[Channel]:
        """Retrieve metadata for the 'find' route."""
        nds2_channels = nds2.find_channels(
            pattern,
            nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_ONLINE,
            min_sample_rate=min_rate,
            max_sample_rate=max_rate,
            params=self._params,
        )
        return [
            Channel(
                channel.name,
                data_type=_NDS2_DATA_TYPE[channel.data_type],
                sample_rate=int(channel.sample_rate),
            )
            for channel in nds2_channels
        ]

    def count(
        self,
        *,
        pattern: str,
        data_type: list[str],
        min_rate: int,
        max_rate: int,
        publisher: list[str],
    ) -> int:
        """Retrieve metadata for the 'count' route."""
        conn = None
        try:
            conn = nds2.connection(self._params)
            return conn.count_channels(
                pattern,
                nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_ONLINE,
                nds2.channel.DEFAULT_DATA_MASK,
                min_rate,
                max_rate,
            )
        finally:
            if conn is not None:
                conn.close()

    def describe(self, *, channels: Iterable[str]) -> Iterable[Channel]:
        """Retrieve metadata for the 'describe' route."""
        return self._query_metadata(channels)

    def stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> Iterator[SeriesBlock]:
        """Retrieve timeseries data for the 'fetch' route."""
        is_live = not start and not end
        if is_live:
            stream = nds2.iterate(
                channels, stride=nds2.connection.FAST_STRIDE, params=self._params
            )
        else:
            stream = nds2.iterate(
                channels,
                start,
                end,
                stride=nds2.connection.FAST_STRIDE,
                params=self._params,
            )
        yield from self._process_stream(stream)

    def _process_stream(self, stream) -> Iterator[SeriesBlock]:
        for buffers in stream:
            channel_data = {}
            channel_dict = {}
            timestamp = 0
            for buf in buffers:
                channel = buf.channel
                timestamp = buf.gps_seconds * Time.SECONDS + buf.gps_nanoseconds
                channel_data[channel.name] = buf.data
                channel_dict[channel.name] = Channel(
                    channel.name,
                    _NDS2_DATA_TYPE[channel.data_type],
                    channel.sample_rate,
                )

            yield SeriesBlock(timestamp, channel_data, channel_dict)

    def _query_metadata(self, channels: Iterable[str]) -> list[Channel]:
        buffers = next(nds2.iterate(channels, 0, 1, params=self._params))
        return [
            Channel(
                buf.channel.name,
                data_type=_NDS2_DATA_TYPE[buf.channel.data_type],
                sample_rate=buf.channel.sample_rate,
            )
            for buf in buffers
        ]
