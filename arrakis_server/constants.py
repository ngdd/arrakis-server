# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE


DEFAULT_LOCATION = "grpc://0.0.0.0:31206"

FLIGHT_REUSE_URL = "arrow-flight-reuse-connection://?"
